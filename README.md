# Canaan Reborn Open Source 2022 (last version)

# What is this?
Two years ago, I started this project without any clue on how big it was going to be. I started it out of my sole passion for games and development. A couple weeks later, many of you joined me on this journey. I was also not alone anymore working on the project; the following people joined the inner staff : 
- Devline
- YaelleDrev
- Canaima
- Sazan
- Alex
- Albert
- MoAngus

During two years, we have worked on the project, started it over a couple times and we had lots of fun working on the game. However, we decided to stop. Truth be told, I decided to stop working on this project. 

Canaan Online / Pet forest was a wonderful game, yet full of features. I've been the only developer on this game for the past two years and it was fun. I learned many things, developed lots of things, had a blast implementing my ideas. Nevertheless, after 2 years developing the game, I am not feeling any more fun developing *this game* nor have the time to commit to a project this big. We're talking a months/years of development for a fully funded team with at least 5-10 developers working full time. We clearly lack the resources to get further and I tried recruiting more developers but finding developers working for free is hard, finding good developers working for free is harder, finding good developers working a lot for free is even harder. 

I started counting the hours I spent working on this project in 2020. I stopped in March 2021 after reaching more than a hundred hours. We can clearly say that I gave a couple hundreds of hours of my time developing the game and it is still not out yet. It is not far from releasing into an alpha but I lack the motivation and the will to go further. I enjoyed my times working on this project and I am grateful that I could be the one reviving this game. Now is the time to take a decision. I decided, with the help of the team, to release the entire Canaan Reborn project source code along with the resources we used and we developed. We hope that someone will take the project further and maybe we could see many canaan reborn games in a couple months. Before diving into the source code, I'd like to thank all of you, without whom we could have not come this far. 

Thank you all for your support all this time. A special thanks to all our financial supporters. Take care,

# What will happen next ?
With the rest of the team, we might start developing another game. We don't know yet anything about it, we're still in the brainstorming phase but we hope you will keep supporting us. We'd like to keep this discord as it is for now, we will come back to you with more info soon.

# Licenses
The whole archive is following the MIT License with the following additional restrictions :
- The source code (aka all files that contain code in either gdscript, typescript, python, javascript, haxe, or any other programming language) belongs to Zekrose. However, the MIT License applies to it, meaning that you can do anything with it as long as you comply to these additional conditions:
    - Commercial use: You are not allowed to reuse this code for any commercial use. You can not sell it, make money out of it, lend it nor buy it. If you wish to do so, please contact us before and we will find an agreement.
    - Mention the original author (Zekrose)
- All the visual assets (images, videos, gifs, etc.) mentioned hereunder belong to YaelleDrev. You can use them in any way except that you must credit the original author and you must not use it for commercial purposes. If you wish to do so, please contact us before and we will find an agreement.
    - All .png files in ./client/assets/skills/ except "mage_1.png" and "mage_2.png"
    - The logo.png file in ./client/assets/ui/general/
    - All .png files in ./client/assets/ui/overworld/
- The canaan original archive containing the original game's assets (2011) is Licensed to the original copyrights holders of the game, aka XPEC Entertainment. However, due to their long absence and several fraud in China (+ bankruptcy), this license might be void. We therefore are not responsible for any of your use of these assets. You should consult a Lawyer in your country before using those assets, as we did in our country. 

# Project content
- canaanoriginal: Original assets of the 2011 version of the game
- client-main: Client assets with source code
- server-master: Server assets with source code
- tools-main: Various tools and utilities to create content for the game, extract data, etc.
- canaanreborn-c++-october2020: Canaan Reborn version developed in C++ (outdated)
- canaanreborn2020prealpha: Canaan Reborn pre-alpha version in Summer 2020. Developed in Haxe
